Tools used:
    MPLABX v5.25
    XC32 v2.30

If an earlier version of MPLABX is being used then it may be easier
to create a new project:

1. Create a new project for processor PIC32MZ2048EFM144
2. Add all the source contents of 'boot' to the project
3. Add the linker file p32MZ2048EFM144_boot.ld
4. Add the startup code crt0_boot.S
5. Edit the project properties:
   + Building: Tick "Normalize hex file"
   + XC32 (Global Options)
     Disable "Use Legacy libc"
   + xc32-gcc
     Tick "Exclude floating point library"
     Add the following to "Additional Options":
       -std=c99 -G2 -mlong-calls
     You may also want to add hordes of warnings:
       -Wall -Wextra -Wformat -Winit-self -Wmissing-include-dirs -Wswitch-enum
       -Wuninitialized -Wfloat-equal -Wshadow -Wpointer-arith -Wundef
       -Wbad-function-cast -Wcast-qual -Wcast-align -Wconversion -Wlogical-op
       -Waggregate-return -Wstrict-prototypes -Wmissing-prototypes
       -Wnormalized=nfc -Wpadded -Wredundant-decls
    + xc32-ld
      [libraries]
      Tick "Do not link crt0 startup code"
      Tick "Exclude foating-point library"


Testing that the boot and main apps work as intended:
1. merge the boot and main hex files - this is easiest to do by copying the
   *.hex files from the production directory to a temporary directory and naming
   then "boot.hex" and "main.hex". Hexmate can merge the files:
   hexmate boot.hex main.hex -format=INHX32,32 -Ocombined.hex
   The hexmate tool is in the MPLABX installation directory, for example:
   MPLABX\v5.25\mplab_platform\bin

2. Program the merged hex file using the IDE and a PICKit (or similar tool).

3. Observe the green LED.  If the bootloader is functioning the LED should blink
   at a rate of once per 2 seconds.  If the bootloader is OK then press the
   pushbutton for half a second; this should chain to the main application.
   If the main application is functioning then the LED should blink at a rate
   of two times per second.
