/*
 * File  : cfg_interrupts.h
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 15 Aug 2020
 *
 * Description: assignments of interrupt priorities and shadow registers
 *              SRS_xxx = one of IPLnSRS, IPLnAUTO, IPLnSOFT
 *              IPL_xxx = Interrupt Priority Level, 0..7
 *              IPS_xxx = Interrupt Priority Sublevel, 0..3
 */


#ifndef CFG_INTERRUPTS_H
#define CFG_INTERRUPTS_H

/* Timer / Counters */
#define SRS_TIMER1      IPL7SRS
#define IPL_TIMER1      7
#define IPS_TIMER1      3

#endif /* CFG_INTERRUPTS_H */
