/*
 * File  : sys_util.c
 * Author: Cirilo Bernardo (cirilo.bernardo@gmail.com)
 * Date  : 15 Aug 2020
 *
 * Description: various system tools for the bootloader
 */

#include <xc.h>
#include <sys/attribs.h>
#include <stdint.h>

#include "sys_util.h"
#include "cfg_interrupts.h"

/******************************************************************************/
/*               PRIVATE VARIABLES                                            */
/******************************************************************************/
static uint32_t m_millisecTime;


/******************************************************************************/
/*               FUNCTION DECLARATIONS                                        */
/******************************************************************************/
void  Timer1_Handler(void);



/******************************************************************************/
/*               PUBLIC FUNCTIONS                                             */
/******************************************************************************/

/**
 * @Function sys_init
 * @detail   Initializes system utilities.
 */
void  sys_init(void)
{
    /*
     * Initialize Timer 1 for 1ms interrupts
     * Registers:
     *   T1CON = Control
     *   TMR1  = Value
     *   PR1   = MAX period
     *   IFS0<4> = Interrupt Flag Set
     *   IEC0<4> = Interrupt Enable Configuration
     *   IPC1<4:2> = IPL
     *   IPC1<1:0> = IPS
     *   Required prescaler: 0x01 = 1:8
     */
    T1CON = 0;
    T1CONbits.TCKPS = 1;
    PR1 = (uint32_t)((sys_get_pbclk() / (1000 * 8)) - 1);
    TMR1 = 0;
    IPC1bits.T1IP = IPL_TIMER1;
    IPC1bits.T1IS = IPS_TIMER1;
    IFS0bits.T1IF = 0;
    T1CONbits.ON = 1;
    IEC0bits.T1IE = 1;

    return;    
}

/**
 * @Function sys_millisec_time
 * @detail   Returns the value of the system millisecond timer.
 */
uint32_t  sys_millisec_time(void)
{
    return m_millisecTime;
}

/**
 * @Function sys_millisec_time
 * @detail   Returns the value of the system millisecond timer.
 */
bool  sys_timeout_ms(uint32_t const startTime, uint32_t const timeout)
{
    bool result = false;

    if ((m_millisecTime - startTime) >= timeout)
    {
        result = true;
    }

    return result;
}


/**
 * @Function sys_addr_to_phy
 * @detail   Returns a physical address given a virtual address in KSEG0 or KSEG1.
 */
void *sys_addr_to_phy(void const *addr)
{
    /*
     * Note: this conversion assumes that EBI and SQI mapping are not used
     */
    return (void *)(((uint32_t)addr) & 0x1FFFFFFF);
}


/**
 * @Function Timer1_Handler
 * @detail   Interrupt Service Routine for the system millisecond timer.
 */
void  __ISR(_TIMER_1_VECTOR, SRS_TIMER1) Timer1_Handler(void)
{
    IFS0bits.T1IF = 0;
    ++m_millisecTime;
    return;
}
