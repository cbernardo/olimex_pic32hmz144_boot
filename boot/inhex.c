/*
 * File  : inhex.c
 * Date  : 17 Aug 2020
 * Description: decoder for Intel Hex format files
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */


/*
 * Implementation notes:
 * a. Microchip UART bootloader protocol drops the leading ':' from
 *    each line in the hex file and converts the remainder of the
 *    line from ASCII into a binary stream prior to transmission.
 *
 * b. Each line of a hex record consists of the following segments:
 *    + SIZE : 1B : size of the decoded DATA segment in bytes
 *    + ADDR : 2B : 16-bit offset to apply to current base address;
 *                  ignored in all but a DATA record. MSB first.
 *    + TYPE : 1B : Record Type
 *    + DATA : xB : data payload, SIZE bytes
 *    + CSUM : 1B : cheksum, single byte
 *
 * c. Record Types: (only types required for PIC32 hex files)
 *    + 0x00 : data to be programmed
 *    + 0x01 : end of file (signals end of programming)
 *    + 0x04 : extended linear address; payload is the upper 16 bits
 *             of the 32-bit Base Address. Order is MSB first.
 *
 * d. Checksum algorithm: sum all bytes from SIZE through DATA and
 *    take the two's complement of the LSB. With this scheme, adding
 *    the checksum byte to the current sum will result in zero.
 */

/* Note:
 * The best way to debug this bit of code would be to make up a dummy framework
 * and feed it known inputs and test the outputs against what is expected.
 * Debugging in conjunction with the remote terminal and in the absence of a
 * second UART dedicated to debug messages is not a reasonable task.
 */

#include <stdint.h>

#include "cfg_programmer.h"
#include "inhex.h"
#include "mchp_crc.h"
#include "nvmem.h"
#include "sys_err.h"

static uint32_t m_row0[(CFG_NVMEM_SIZE_ROW >> 2)] __attribute__((__coherent__, __aligned__ (16)));
static uint32_t m_rowN[(CFG_NVMEM_SIZE_ROW >> 2)] __attribute__((__coherent__, __aligned__ (16)));

static uint32_t m_baseAddress;              /* upper 16 bits of address */
static uint32_t m_rowAddress;               /* 32-bit address of the current row */
static uint32_t m_currentAddress;           /* current address to be programmed */

static int32_t  process_data(uint8_t const *record);
static void     init_row(uint32_t *row);

/**
 * @Function inhex_erase
 * @detail   Erases the entire Program Memory region.
 * @return   0 on success, else error code.
 */
int32_t inhex_erase(void)
{
    int32_t result = 0;

    if (0 == (CFG_PROG_PAGE_WP))
    {
        /* entire program memory can be erased in one operation */
        result = nvm_erase_prog();
    }
    else
    {
        /* part of program memory must be erased by pages */
        uint32_t startAddr = (CFG_PROG_PAGE_WP) + (CFG_PROG_ADDR_START);
        uint32_t endAddr = (CFG_PROG_BANK1_ADDR);

        if (startAddr >= (CFG_PROG_BANK1_ADDR))
        {
            endAddr = (CFG_PROG_ADDR_END);
        }

        while ((0 == result) && (startAddr < endAddr))
        {
            result = nvm_erase_page(startAddr);
            startAddr += (CFG_NVMEM_SIZE_PAGE);
        }

        if ((0 == result) && (endAddr != (CFG_PROG_ADDR_END)))
        {
            /* bank 1 may be erased in one operation */
            result = nvm_erase_bank(1);
        }
    }

    return result;
} /* inhex_erase */

/**
 * @Function inhex_process
 * @detail   Processes an Intel Hex record; the program memory must have been
 *           erased in a previous operation and the address must be in a
 *           monotonically increasing order. Addresses outside the Program
 *           Flash range are ignored and a value of zero is returned.
 * @param    record is a pointer to the binary equivalent of the Intel Hex record
 * @param    len is the total length of the record including the checksum byte
 * @return   0 on success, else error code.
 */
int32_t inhex_process(uint8_t const *record, int32_t const len)
{
    int32_t result = 0;

    if (5 > len)
    {
        /* invalid HEX record; min. length is 5 bytes for the overhead */
        result = E_FAIL;
    }
    else if (len != (*record + 5))
    {
        /* invalid HEX record; total length is inconsistent with payload + overhead */
        result = E_FAIL;
    }

    if (0 == result)
    {
        /* ensure checksum = 0 */
        uint32_t cksum = 0;
        int32_t  i;

        for (i = 0; i < len; ++i)
        {
            cksum += record[i];
        }

        if (0 != (cksum & 0xFFUL))
        {
            /* checksum mismatch */
            result = E_FAIL;
        }
    }

    if (0 == result)
    {
        /* process the record if it is a supported type */
        if (record[3] == 0x00)
        {
            /* 0x00 : data to be programmed */
            result = process_data(record);
        }
        else if (record[3] == 0x01)
        {
            /* 0x01 : end of file */
            result = inhex_finalize();
        }
        else if (record[3] == 0x04)
        {
            /* 0x04 : extended linear address */
            uint32_t baseAddr = ((((uint32_t)record[4]) << 8) | (uint32_t)(record[5])) << 16;

            if ((baseAddr >= (CFG_PROG_ADDR_START)) && (baseAddr < CFG_PROG_ADDR_END))
            {
                if (baseAddr < m_baseAddress)
                {
                    /* address is not monotonically increasing */
                    result = E_FAIL;
                }
            }

            m_baseAddress = baseAddr;
        }
    }

    return result;
} /* inhex_process */

/**
 * @Function inhex_finalize
 * @detail   Finalizes the programming operation.
 * @return   0 on success, else error code.
 */
int32_t inhex_finalize(void)
{
    int32_t  result = 0;
    uint32_t startAddr = ((CFG_PROG_ADDR_START) + (CFG_PROG_PAGE_WP));

    if ((m_rowAddress != m_currentAddress) && (m_rowAddress != startAddr))
    {
        result = nvm_prog_row(m_rowAddress, m_rowN);
        m_rowAddress += (CFG_NVMEM_SIZE_ROW);
        m_currentAddress = m_rowAddress;
    }

    if (m_row0[0] != 0xDEADD0D0)
    {
        result = nvm_prog_row(((CFG_PROG_ADDR_START) + (CFG_PROG_PAGE_WP)), m_row0);

        if (0 == result)
        {
            /* data was successfully programmed; invalidate the buffer */
            m_row0[0] = 0xDEADD0D0;
            m_baseAddress = 0;
            m_rowAddress = 0;
            m_currentAddress = 0;
        }
    }

    return result;
} /* inhex_finalize */

/**
 * @Function inhex_calc_crc
 * @detail   Calculates a CRC-16 value over the Physical Memory range given by the
 *           start address and length.
 * @param    startAddr is an address in the region CFG_PROG_ADDR_START to CFG_PROG_ADDR_END
 *           or the equivalents in the virtual address space.
 * @param    length    is the number of bytes to process.
 * @return   0 on error conditions, otherwise calculated CRC which may also be zero.
 */
int32_t inhex_calc_crc(uint32_t startAddr, uint32_t const length)
{
    int32_t result = 0;
    uint32_t progAddrStart = 0xA0000000UL | (CFG_PROG_ADDR_START); /* non-cache */
    uint32_t progAddrEnd = 0xA0000000UL | (CFG_PROG_ADDR_END);
    startAddr |= 0xA0000000UL;
    uint32_t endAddr = startAddr + length;

    /* note: the remote terminal can request any address range but anything outside
       (CFG_PROG_ROW0_ADDR) to (CFG_PROG_ADDR_END) would not be programmed and the
       CRC should fail (if we're not unlucky)
     */

    if ((startAddr >= progAddrStart) && (startAddr <= progAddrEnd)
        && (endAddr <= (progAddrEnd + 1UL))
        && (length <= (1UL + ((CFG_PROG_ADDR_END) - (CFG_PROG_ADDR_START)))))
    {
        uint32_t row0end = (0xA0000000UL | ((CFG_PROG_ROW0_ADDR) + (CFG_NVMEM_SIZE_ROW)));

        if ((0xDEADD0D0 != m_row0[0]) && (startAddr >= (0xA0000000UL | (CFG_PROG_ROW0_ADDR)))
            && (startAddr < row0end))
        {
            /* special case: falls within first programmable row and row has not been programmed */
            if ((startAddr + length) <= row0end)
            {
                /* all addresses are within the first row */
                result = CalculateCrc(0, (uint8_t const *)m_row0, length);
            }
            else
            {
                /* first (row0end - startAddr) bytes in m_row0, the rest starts at row0end */
                uint32_t extra = (startAddr + length) - row0end;
                result = CalculateCrc(0, (uint8_t const *)m_row0, (row0end - startAddr));
                result = CalculateCrc((uint16_t)result, (uint8_t const *)row0end, extra);
            }
        }
        else
        {
            result = CalculateCrc(0, (uint8_t const *)startAddr, length);
        }
    }

    return result;
} /* inhex_calc_crc */


/**
 * @Function process_data
 * @detail   Processes a Data Record and programs a row of data as necessary.
 * @param    record is a pointer to the binary equivalent of the Intel Hex record.
 * @return   0 on success, else error code.
 */
static int32_t  process_data(uint8_t const *record)
{
    int32_t  result = 0;
    uint32_t progAddr = (((uint32_t)record[1] << 8) | (uint32_t)record[2]) | m_baseAddress;
    uint32_t rowAddr = (progAddr & ~((CFG_NVMEM_SIZE_ROW) - 1UL));

    /* note: addresses outside the designated programmable range are ignored */
    if ((progAddr >= (CFG_PROG_ROW0_ADDR)) && (progAddr <= (CFG_PROG_ADDR_END)))
    {
        if (progAddr < m_currentAddress)
        {
            result = E_FAIL;
        }
        else
        {
            if (0 == m_rowAddress)
            {
                m_rowAddress = rowAddr;
                m_currentAddress = rowAddr;
                init_row(m_row0);
                init_row(m_rowN);
            }
            else if (rowAddr != m_rowAddress)
            {
                if (m_rowAddress != (CFG_PROG_ROW0_ADDR))
                {
                    /* program the previous row */
                    result = nvm_prog_row(m_rowAddress, m_rowN);
                    init_row(m_rowN);
                }

                m_rowAddress = rowAddr;
                m_currentAddress = rowAddr;
            }
        }

        if (0 == result)
        {
            uint32_t  idx = progAddr - m_rowAddress;
            uint32_t  len = record[0];
            uint8_t  *row = (uint8_t *)m_rowN;
            uint8_t  const *src = &record[4];

            if (m_rowAddress == (CFG_PROG_ROW0_ADDR))
            {
                row = (uint8_t *)m_row0;
            }

            while ((0 < len) && (idx < (CFG_NVMEM_SIZE_ROW)))
            {
                row[idx++] = *src++;
                ++progAddr;
                --len;
            }

            if (0 < len)
            {
                /* program a row and continue */
                if (m_rowAddress != (CFG_PROG_ROW0_ADDR))
                {
                    /* program the current row */
                    result = nvm_prog_row(m_rowAddress, m_rowN);
                    init_row(m_rowN);
                    m_rowAddress += (CFG_NVMEM_SIZE_ROW);
                    m_currentAddress = m_rowAddress;
                }
                else
                {
                    /* the special buffer m_row0 has been filled; move on to m_rowN */
                    row = (uint8_t *)m_rowN;
                }

                /* copy remainder of data to the new row */
                idx = 0;
                while (0 < len)
                {
                    row[idx++] = *src++;
                    ++progAddr;
                    --len;
                }
            }

            m_currentAddress = progAddr;
        }
    }

    return result;
}

/**
 * @Function init_row
 * @detail   Initializes a row buffer with 0xFF.
 * @param    row is a pointer to the buffer to be initialized.
 */
static void  init_row(uint32_t *row)
{
    uint32_t i;

    for (i = 0; i < ((CFG_NVMEM_SIZE_ROW) >> 2); ++i)
    {
        row[i] = 0xFFFFFFFFUL;
    }

    return;
}
