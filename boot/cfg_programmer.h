/*
 * File  : cfg_programmer.h
 * Date  : 18 Aug 2020
 * Description: provides configuration parameters for the bootloader
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#ifndef CFG_PROGRAMMER_H
#define CFG_PROGRAMMER_H

/* UART communications speed */
#define CFG_CONSOLE_SPEED       (115200UL)

/* Program Memory Page and Row Sizes (bytes) */
#define CFG_NVMEM_SIZE_PAGE     (16384UL)   /* PIC32MZ-EF series page size */
#define CFG_NVMEM_SIZE_ROW      (2048UL)    /* PIC32MZ-EF series row size */

/* Program Flash Write Protect Boundary (24 bit address; align to page).
 * Set to a non-zero page value if the bootloader requires space in Program Memory.
 */
#define CFG_PROG_PAGE_WP        (0x000000UL)

#if (0 != ((CFG_PROG_PAGE_WP) & ((CFG_NVMEM_SIZE_PAGE) - 1)))
#error Program Memory Page Write Protect is not aligned to a Page
#endif

/* First and last Physical Addresses for the Program Flash */
#define CFG_PROG_ADDR_START     (0x1D000000UL)
#define CFG_PROG_ADDR_END       (0x1D1FFFFFUL)

/* Boundaries for the Program Memory Banks */
#define CFG_PROG_BANK0_ADDR     (CFG_PROG_ADDR_START)
#define CFG_PROG_BANK1_ADDR     (0x1D100000UL)

/* Address of first programmable row */
#define CFG_PROG_ROW0_ADDR      ((CFG_PROG_ADDR_START) + (CFG_PROG_PAGE_WP))

#if ((CFG_PROG_PAGE_WP) > ((CFG_PROG_ADDR_START) & 0xFFFFFFUL))
#error Program Memory Page Write Protect overlaps Program Start Address
#endif

#if (CFG_PROG_ADDR_END < ((CFG_PROG_ADDR_START) + (CFG_NVMEM_SIZE_PAGE)))
#error No memory available for the main application
#endif

#endif /* CFG_PROGRAMMER_H */
