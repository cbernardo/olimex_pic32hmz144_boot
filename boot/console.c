/*
 * File  : console.c
 * Date  : 16 Aug 2020
 * Description: provides a serial console and print functions
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <proc/p32mz2048efm144.h>

#include "cfg_programmer.h"
#include "console.h"
#include "sys_err.h"
#include "uart.h"


static UART_Driver *m_uart = &UART2;    /* map m_uart to the console UART */
static int32_t      m_ccstate;          /* state variable for character conversion */

static int32_t  con_init(void);
static void     con_uninit(void);
static void     con_write(void const *message, uint32_t const length);
static void     con_puts(void const *message);
static void     con_putc(const uint8_t token);
static int32_t  con_getc(void);
static void     con_flushtx(void);
static void     con_flushrx(void);
static void     con_print32H(uint32_t const val);

Console_t Console =
{
    .Init = con_init,
    .Uninit = con_uninit,
    .Write = con_write,
    .Puts = con_puts,
    .Putc = con_putc,
    .Getc = con_getc,
    .FlushTx = con_flushtx,
    .FlushRx = con_flushrx,
    .Print32H = con_print32H,
};


void console_task(void)
{
    if (!m_uart->IsActive())
    {
        m_uart->Init(CFG_CONSOLE_SPEED);
    }

    uart_task();

    /* Note: process data here if required */
    return;
} /* console_task */


static int32_t con_init(void)
{
    return m_uart->Init(CFG_CONSOLE_SPEED);
} /* con_init */

static void con_uninit(void)
{
    m_uart->Uninit();
    return;
} /* con_uninit */

static void con_write(void const *message, uint32_t const length)
{
    if (!m_uart->IsActive())
    {
        m_uart->Init(CFG_CONSOLE_SPEED);
    }

    if ((0 != message) && (0 != length) && (m_uart->IsActive()))
    {
        (void)UART2.Send(message, length);
    }

    return;
} /* con_write */

static void  con_puts(void const *message)
{
    if (!m_uart->IsActive())
    {
        m_uart->Init(CFG_CONSOLE_SPEED);
    }

    if ((0 != message) && (m_uart->IsActive()))
    {
        uint8_t const *pdata = (uint8_t const *)message;

        while (0 != *pdata)
        {
            if ('\n' == *pdata)
            {
                if (1 != m_ccstate)
                {
                    m_uart->Send("\r\n", 2);
                }

                m_ccstate = 0;
            }
            else if ('\r' == *pdata)
            {
                if (1 == m_ccstate)
                {
                    m_uart->Send("\r\n", 2);
                }

                m_ccstate = 1;
            }
            else
            {
                m_ccstate = 0;
                m_uart->Send(pdata, 1);
            }

            ++pdata;
        }
    }

    return;
} /* con_puts */


static void  con_putc(const uint8_t token)
{
    if (!m_uart->IsActive())
    {
        m_uart->Init(CFG_CONSOLE_SPEED);
    }

    if (m_uart->IsActive())
    {
        while (0 == m_uart->Status().tx_underrun)
        {
            uart_task();
        }

        m_uart->Send(&token, 1);
    }

    return;
} /* con_putc */


static int32_t  con_getc(void)
{
    if (!m_uart->IsActive())
    {
        m_uart->Init(CFG_CONSOLE_SPEED);
    }

    uint8_t token = 0;
    int32_t result = m_uart->Receive(&token, 1);

    if (0 == result)
    {
        result = E_FAIL;
    }
    else
    {
        result = token;
    }

    return result;
} /* con_getc */

static void  con_flushtx(void)
{
    m_uart->FlushTx();
    return;
} /* con_flushtx */

static void     con_flushrx(void)
{
    m_uart->FlushRx();
    return;
} /* con_flushrx */


static void con_print32H(uint32_t val)
{
    char msg[9];
    msg[8] = 0;
    int32_t i;
    int32_t shift = 28;

    for (i = 0; i < 8; ++i)
    {
        int32_t tok = (int32_t)((val >> shift) & 0x0F) + 48;

        if (tok > 57)
        {
            tok += 7;
        }

        msg[i] = (char)tok;
        shift -= 4;
    }

    con_puts(msg);
}
