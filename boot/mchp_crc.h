/*
 * File  : mchp_crc.c
 * Source: Adapted from Microchip AN1388 source file Framework.c
 * Description: CRC-16 calculator for the PIC32 bootloader protocol.
 *   CRC parameters such as the polynomial are not known but the initial
 *   value is clearly zero.
 */

#ifndef MCHP_CRC_H
#define MCHP_CRC_H

#include <stdint.h>

uint16_t CalculateCrc(uint16_t const init_crc, uint8_t const *data, uint32_t len);

#endif /* MCHP_CRC_H */
