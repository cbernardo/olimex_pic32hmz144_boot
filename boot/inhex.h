/*
 * File  : inhex.h
 * Date  : 17 Aug 2020
 * Description: decoder for Intel Hex format files
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#ifndef INHEX_H
#define INHEX_H

#include <stdint.h>

int32_t inhex_erase(void);
int32_t inhex_process(uint8_t const *record, int32_t const len);
int32_t inhex_finalize(void);
int32_t inhex_calc_crc(uint32_t startAddr, uint32_t const length);

#endif /* INHEX_H */
