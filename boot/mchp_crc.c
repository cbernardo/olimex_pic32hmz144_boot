/*
 * File  : mchp_crc.c
 * Source: Adapted from Microchip AN1388 source file Framework.c
 * Description: CRC-16 calculator for the PIC32 bootloader protocol.
 *   CRC parameters such as the polynomial are not known but the initial
 *   value is clearly zero.
 */

#include <stdint.h>
#include "mchp_crc.h"

static const uint16_t crc_table[16] =
{
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef
};

uint16_t CalculateCrc(uint16_t const init_crc, uint8_t const *data, uint32_t len)
{
    uint16_t tmp;
    uint16_t crc = init_crc;

    while(len--)
    {
        tmp = (uint16_t)((crc >> 12) ^ (*data >> 4));
        crc = (uint16_t)(crc_table[tmp & 0x0F] ^ (crc << 4));
        tmp = (uint16_t)((crc >> 12) ^ (*data));
        crc = (uint16_t)(crc_table[tmp & 0x0F] ^ (crc << 4));
        data++;
    }

    return crc;
}
