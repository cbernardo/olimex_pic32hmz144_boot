/*
 * File  : nvmem.h
 * Date  : 17 Aug 2020
 * Description: non-volatile memory controller for Program Flash.
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#ifndef BOOT_NVMEM_H
#define BOOT_NVMEM_H

#include <stdint.h>

int32_t  nvm_erase_page(uint32_t addr);
int32_t  nvm_erase_bank(uint32_t bank);
int32_t  nvm_erase_prog(void);
int32_t  nvm_prog_row(uint32_t addr, void const *data);
void     nvm_nop(void);

#endif /* BOOT_NVMEM_H */
