/*
 * File  : mcu_config.c
 * Date  : 14 Aug 2020
 * Description: Microprocessor configuration settings and bootup pin configuration
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

/* NOTE: As per Microchip's advice do not include any files prior to the config pragmas */

// DEVCFG3
// USERID = No Setting
#pragma config FMIIEN = ON              // Ethernet RMII/MII Enable (MII Enabled)
#pragma config FETHIO = ON              // Ethernet I/O Pin Select (Default Ethernet I/O)
#pragma config PGL1WAY = OFF            // Permission Group Lock One Way Configuration (Allow multiple reconfigurations)
#pragma config PMDL1WAY = OFF           // Peripheral Module Disable Configuration (Allow multiple reconfigurations)
#pragma config IOL1WAY = OFF            // Peripheral Pin Select Configuration (Allow multiple reconfigurations)
#pragma config FUSBIDIO = OFF           // USB USBID Selection (Controlled by Port Function)

// DEVCFG2
#pragma config FPLLIDIV = DIV_3         // System PLL Input Divider (3x Divider)
#pragma config FPLLRNG = RANGE_5_10_MHZ // System PLL Input Range (5-10 MHz Input)
#pragma config FPLLICLK = PLL_POSC      // System PLL Input Clock Selection (POSC is input to the System PLL)
#pragma config FPLLMULT = MUL_50        // System PLL Multiplier (PLL Multiply by 50)
#pragma config FPLLODIV = DIV_2         // System PLL Output Clock Divider (2x Divider)
#pragma config UPLLFSEL = FREQ_24MHZ    // USB PLL Input Frequency Selection (USB PLL input is 24 MHz)

// DEVCFG1
#pragma config FNOSC = SPLL             // Oscillator Selection Bits (System PLL)
#pragma config DMTINTV = WIN_127_128    // DMT Count Window Interval (Window/Interval value is 127/128 counter value)
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disable SOSC)
#pragma config IESO = OFF               // Internal/External Switch Over (Disabled)
#pragma config POSCMOD = HS             // Primary Oscillator Configuration (External clock mode)
#pragma config OSCIOFNC = OFF           // CLKO Output Signal Active on the OSCO Pin (Disabled)
#pragma config FCKSM = CSECME           // Clock Switching and Monitor Selection (Clock Switch Enabled, FSCM Enabled)
#pragma config WDTPS = PS1048576        // Watchdog Timer Postscaler (1:1048576)
#pragma config WDTSPGM = STOP           // Watchdog Timer Stop During Flash Programming (WDT stops during Flash programming)
#pragma config WINDIS = NORMAL          // Watchdog Timer Window Mode (Watchdog Timer is in non-Window mode)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (WDT Disabled)
#pragma config FWDTWINSZ = WINSZ_25     // Watchdog Timer Window Size (Window size is 25%)
#pragma config DMTCNT = DMT31           // Deadman Timer Count Selection (2^31 (2147483648))
#pragma config FDMTEN = OFF             // Deadman Timer Enable (Deadman Timer is disabled)

// DEVCFG0
#pragma config DEBUG = OFF              // Background Debugger Enable (Debugger is disabled)
#pragma config JTAGEN = OFF             // JTAG Enable (JTAG Disabled)
#pragma config ICESEL = ICS_PGx2        // ICE/ICD Comm Channel Select (Communicate on PGEC1/PGED1)
#pragma config TRCEN = OFF              // Trace Enable (Trace features in the CPU are disabled)
#pragma config BOOTISA = MIPS32         // Boot ISA Selection (Boot code and Exception code is MIPS32)
#pragma config FECCCON = OFF_UNLOCKED   // Dynamic Flash ECC Configuration (ECC and Dynamic ECC are disabled (ECCCON bits are writable))
#pragma config FSLEEP = OFF             // Flash Sleep Mode (Flash is powered down when the device is in Sleep mode)
#pragma config DBGPER = PG_ALL          // Debug Mode CPU Access Permission (Allow CPU access to all permission regions)
#pragma config SMCLR = MCLR_NORM        // Soft Master Clear Enable bit (MCLR pin generates a normal system Reset)
#pragma config SOSCGAIN = GAIN_2X       // Secondary Oscillator Gain Control bits (2x gain setting)
#pragma config SOSCBOOST = OFF           // Secondary Oscillator Boost Kick Start Enable bit (Boost the kick start of the oscillator)
#pragma config POSCGAIN = GAIN_2X       // Primary Oscillator Gain Control bits (2x gain setting)
#pragma config POSCBOOST = OFF           // Primary Oscillator Boost Kick Start Enable bit (Boost the kick start of the oscillator)
#pragma config EJTAGBEN = NORMAL        // EJTAG Boot (Normal EJTAG functionality)
#pragma config CP = OFF                 // Code Protect (Protection Disabled)


/* Include headers */
#include <xc.h>
#include "cfg_programmer.h"

/* Forward declarations */
extern void _on_reset(void);


/* Functions Definitions */

/**
 * @Function _on_reset
 * @detail   Invoked by the startup code after global pointer initialization
 *           but prior to CP0 configuration and C static initialization. This is
 *           where the peripheral modules and pin I/O are typically configured
 *           for low power operation.
 */
void _on_reset(void)
{
    /*******************************************/
    /* Flash Memory : protect bootloader pages */
    /*******************************************/
    if (0 != (CFG_PROG_PAGE_WP))
    {
        /* protect bootloader segment in Program Flash */
        NVMKEY = 0;
        NVMKEY = 0xAA996655;
        NVMKEY = 0x556699AA;
        NVMPWP = ((CFG_PROG_PAGE_WP) & 0x00FFFFFFUL);
    }

    NVMKEY = 0;
    NVMKEY = 0xAA996655;
    NVMKEY = 0x556699AA;
    NVMBWP = 0x1F1FUL;              /* Protect all boot flash memory */

    /* unlock device configuration registers */
    SYSKEY = 0;
    SYSKEY = 0xAA996655;
    SYSKEY = 0x556699AA;

    /*************************/
    /* PERIPHERAL PIN SELECT */
    /*************************/

    /* Pin Select, UART 2 */
    U2RXR = 0b1101; /* RE9 = U2RX */
    RPE8R = 0b0010; /* RE8 = U2TX */

    /*****************************/
    /* PERIPHERAL MODULE DISABLE */
    /*****************************/
    /* Notes:
     * - no modules are currently disabled
     * - do not attempt to disable REFCLK (Erratum #5, DS80000663L)
     */

    /* PERIPHERAL BUS CLOCKS
     * No additional configuration required.
     * On Reset the clocks are on and the source is SYSCLK.
     * PBCLK1:6 = SYSCLK / 2
     * PBCLK7   = SYSCLK
     */

    /* REFERENCE CLOCKS
     * No additional configuration required.
     * Defaults are OFF, Source=SYSCLK, Divisor=1.
     */

    /* lock device configuration registers */
    SYSKEY = 0x33333333;

    /**********************************************/
    /* PORT CONFIGURATION FOR OLIMEX PIC32-HMZ144 */
    /**********************************************/

    /* Initialize ADC Pin Configuration Registers (state is unspecified on reset) */
    ANSELA = ANSELB = ANSELC = ANSELD = ANSELE = ANSELF = ANSELG = ANSELH = ANSELJ = 0;

    /* PORT A
     *  0: TMS      OUT(0)
     *  1: TCK      OUT(0)
     *  2: SCL2     IN(X)   Has external pull-up to 3.3V
     *  3: SDA2     IN(X)   Has external pull-up to 3.3V
     *  4: RA4      OUT(0)
     *  5: RA5      OUT(0)
     *  6: RA6      OUT(0)
     *  7: RA7      OUT(0)
     *  9: RA9      OUT(0)
     * 10: AREF     OUT(0)  AREF circuit not populated
     * 14: RA14     OUT(0)  Pull-up for SCL1 not populated
     * 15: RA15     OUT(0)  Pull-up for SDA1 not populated
     */
    ODCA  = 0x0000;
    LATA  = 0x0000;
    CNPUA = 0x0000;
    CNPDA = 0x0000;
    TRISA = 0x000C;     /* 0000 0000 0000 1100 */

    /* PORT B
     *  0: RB0          OUT(0)
     *  1: RB1          OUT(0)
     *  2: OTG_FAULT    IN(X)
     *  3: RB3          OUT(0)
     *  4: RB4          OUT(0)
     *  5: OTG_EN       OUT(0)
     *  6: PGEC2        IN(PD)
     *  7: PGED2        IN(PD)
     *  8: RB8          OUT(0)
     *  9: RB9          OUT(0)
     * 10: RB10         OUT(0)
     * 11: RB11         OUT(0)
     * 12: BUTTON       IN(X)
     * 13: RB13         OUT(0) Active Low
     * 14: #SS2         ODC(1) Has external pull-up to 3.3V
     * 15: #SS1         ODC(1) Has external pull-up to 3.3V
     */
    ODCB  = 0xC000;     /* 1100 0000 0000 0000 */
    LATB  = 0xC000;     /* 1100 0000 0000 0000 */
    CNPUB = 0x0000;
    CNPDB = 0x00C0;     /* 0000 0000 1100 0000 */
    TRISB = 0x10C4;     /* 0001 0000 1100 0100 */

    /* PORT C
     * Note: HMZ144 Rev D has an oscillator on RC12 (POSC) and RC14 (SOSC);
     *       Rev C has a crystal resonator on RC12 + RC15 and  no connection on RC13, RC14.
     *       This configuration is for Rev C.
     *  1: RC1          IN(PD)  Connected off-board to analog source
     *  2: RC2          IN(PD)  Connected off-board to analog source
     *  3: RC3          IN(PD)  Connected off-board to analog source
     *  4: RC4          IN(PD)  Connected off-board to analog source
     * 12: CLKI         IN(X)   Pin config is controlled by Microcontroller Configuration Register
     * 13: RC13         OUT(0)  Not connected (crystal not populated for SOSC)
     * 14: RC14         OUT(0)  Not connected (crystal not populated for SOSC)
     * 15: RC15         IN(X)   Pin config is controlled by Microcontroller Configuration Register
     */
    ODCC  = 0x0000;
    LATC  = 0x0000;
    CNPUC = 0x0000;
    CNPDC = 0x001E;     /* 0000 0000 0001 1110 */
    TRISC = 0x901E;     /* 1001 0000 0001 1110 */

    /* PORT D
     *  0: RD0          OUT(0)
     *  1: SCK1         OUT(0)
     *  2: RD2          OUT(0)
     *  3: RD3          OUT(0)
     *  4: RD4          OUT(0)
     *  5: RD5          OUT(0)
     *  6: RD6          OUT(0)
     *  7: SDI2         IN(X)
     *  9: RD9          OUT(0)
     * 10: RD10         OUT(0)
     * 11: RD11         OUT(0)
     * 12: RD12         OUT(0)
     * 13: RD13         OUT(0)
     * 14: SDI1         IN(PD)  Not connected by default
     * 15: SDO1         OUT(0)
     */
    ODCD  = 0x0000;
    LATD  = 0x0000;
    CNPUD = 0x0000;
    CNPDD = 0x4000;     /* 0100 0000 0000 0000 */
    TRISD = 0x4080;     /* 0100 0000 1000 0000 */

    /* PORT E
     *  0: RE0          OUT(0)
     *  1: RE1          OUT(0)
     *  2: RE2          OUT(0)
     *  3: RE3          OUT(0)
     *  4: RE4          OUT(0)
     *  5: RE5          OUT(0)
     *  6: RE6          OUT(0)
     *  7: RE7          OUT(0)
     *  8: U2TX         OUT(1)  TX idle should be high
     *  9: U2RX         IN(X)   Has external pull-up to 3.3V
     */
    ODCE  = 0x0000;
    LATE  = 0x0100;     /* 0000 0001 0000 0000 */
    CNPUE = 0x0000;
    CNPDE = 0x0000;
    TRISE = 0x0200;     /* 0000 0010 0000 0000 */

    /* PORT F
     *  0: RF0      IN(PD)
     *  1: RF1      IN(PD)
     *  2: RF2      IN(PD)
     *  3: USBID    OUT(0)  Note: USB ID function disabled in DEVCFG
     *  4: RF4      IN(PD)
     *  5: RF5      IN(PD)
     *  8: RF8      OUT(0)
     * 12: TDO      OUT(0)
     * 13: TDI      IN(PD)  Note: not connected by default
     */
    ODCF  = 0x0000;
    LATF  = 0x0000;
    CNPUF = 0x0000;
    CNPDF = 0x2037;     /* 0010 0000 0011 0111 */
    TRISF = 0x2037;     /* 0010 0000 0011 0111 */

    /* PORT G
     *  0: RG0      OUT(0)
     *  1: RG1      OUT(0)
     *  6: SCK2     OUT(0)
     *  7: RG7      OUT(0)  Note: E_MEASUREMENT via 1M resistor. Pull low.
     *  8: SDO2     OUT(1)  Note: SD Card; line pulled high by external resistor.
     *  9: RG9      OUT(0)
     * 12: RG12     OUT(0)
     * 13: RG13     OUT(0)
     * 14: RG14     OUT(0)
     * 15: V_BAT    ODC(1) Note: to 3.7V Li cell via 3M resistor. Relies on internal diode for
     *              over-voltage protection. (!)
     */
    ODCG  = 0x8000;     /* 1000 0000 0000 0000 */
    LATG  = 0x8000;     /* 1000 0000 0000 0000 */
    CNPUG = 0x0000;
    CNPDG = 0x0000;
    TRISG = 0x0000;

    /* PORT H
     *  0: RH0      IN(PD)
     *  1: RH1      IN(PD)
     *  2: LED1     OUT(0)  Green LED; high to turn ON
     *  3: RH3      OUT(0)
     *  4: RH4      OUT(0)
     *  5: RH5      OUT(0)
     *  6: RH6      OUT(0)
     *  7: RH7      OUT(0)
     *  8: RH8      OUT(0)
     *  9: RH9      OUT(0)
     * 10: RH10     OUT(0)
     * 11: RH11     OUT(0)
     * 12: RH12     IN(PD)
     * 13: RH13     IN(PD)
     * 14: RH14     IN(PD)
     * 15: RH15     OUT(0)
     */
    ODCH  = 0x0000;
    LATH  = 0x0000;
    CNPUH = 0x0000;
    CNPDH = 0x7003;     /* 0111 0000 0000 0011 */
    TRISH = 0x7003;     /* 0111 0000 0000 0011 */

    /* PORT J
     *  0: RJ0      OUT(0)
     *  1: RJ1      OUT(0)
     *  2: RJ2      OUT(0)
     *  3: RJ3      OUT(0)
     *  4: RJ4      OUT(0)
     *  5: SD_CD    IN(X)   SD Card Detect (pulled up via 100K to 3.3V)
     *  6: RJ6      OUT(0)
     *  7: RJ7      OUT(0)
     *  8: RJ8      OUT(0)
     *  9: RJ9      OUT(0)
     * 10: RJ10     OUT(0)
     * 11: RJ11     OUT(0)
     * 12: RJ12     OUT(0)
     * 13: RJ13     OUT(0)
     * 14: RJ14     OUT(0)
     * 15: RJ15     OUT(0)
     */
    ODCJ  = 0x0000;
    LATJ  = 0x0000;
    CNPUJ = 0x0000;
    CNPDJ = 0x0000;
    TRISJ = 0x0020;     /* 0000 0000 0010 0000 */

    /* PORT K
     *  0: RK0      OUT(0)
     *  1: RK1      OUT(0)
     *  2: RK2      OUT(0)
     *  3: RK3      OUT(0)
     *  4: RK4      OUT(0)
     *  5: RK5      OUT(0)
     *  6: RK6      OUT(0)
     *  7: RK7      OUT(0)
     */
    ODCK  = 0x0000;
    LATK  = 0x0000;
    CNPUK = 0x0000;
    CNPDK = 0x0000;
    TRISK = 0x0000;

    return;
} /* _on_reset */
