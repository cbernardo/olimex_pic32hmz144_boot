/*
 * File  : main.c
 * Date  : 14 Aug 2020
 * Description: bootloader main entry function
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

#include "cfg_programmer.h"
#include "console.h"
#include "inhex.h"
#include "mchp_crc.h"
#include "nvmem.h"
#include "sys_err.h"
#include "sys_util.h"
#include "uart.h"

/* note: bug in Microchip utility precludes the use of SOH, EOT, DLE */
#define BOOT_MAJOR  (0x02)      /* Major revision, 1..255 */
#define BOOT_MINOR  (0x00)      /* Minor revision, 0..255 */

#define TOK_SOH     (0x01)
#define TOK_EOT     (0x04)
#define TOK_DLE     (0x10)

#define BOOT_WAIT   (0)         /* waiting for SOH from remote terminal */
#define BOOT_ACTIVE (1)         /* processing data from remote terminal */
#define BOOT_FINISH (2)         /* finalize programming and jump */
#define BOOT_JUMP   (3)         /* jump to main requested */

static void (*chain) (void) = (void (*)(void))0xBD000000;   /* startup entry point for main */
static uint8_t m_input[512];                                /* buffer for incoming data */
static int32_t m_index;                                     /* index into input */
static volatile int32_t m_bootState;

static int32_t  boot_task(void);
static void     process_pkt(void);
static void     process_version(void);
static void     process_erase(void);
static void     process_crc(void);
static void     process_write(void);
static void     send_response(uint8_t const *msg, uint32_t length);


/**
 * @Function main
 * @detail   Entry point for bootloader. Note: XC32 compilers don't allow void main(void)
 *           even though this function is never intended to return.
 */
int main(void)
{
    sys_setup();

    /* Send a message to the console */
    Console.Init();
    Console.Puts("** Bootloader initialized\n");

    PORTHSET = (1 << 2);            /* green LED on */
    int32_t  phase = 0;             /* LED status (0 = on) */
    uint32_t blinkTimeout = 1800;
    uint32_t blinkTimer = sys_millisec_time();
    uint32_t bootTimer = sys_millisec_time();
    uint32_t appTag = *((uint32_t *)((0xBD000000UL | (CFG_PROG_ADDR_START)) + 0x20UL));

    if (0x4E49414D != appTag)
    {
        m_bootState = BOOT_ACTIVE;  /* no main; wait indefinitely for the programmer */
    }

    while (1)
    {
        if ((BOOT_WAIT) == m_bootState)
        {
            /* wait up to 1s to receive a <SOH> character */
            console_task();

            if (0 != boot_task())
            {
                m_bootState = BOOT_ACTIVE;
                blinkTimeout = 300;
            }

            if ((BOOT_WAIT == m_bootState) && (sys_timeout_ms(bootTimer, 1000)))
            {
                m_bootState = BOOT_JUMP;
            }
        }
        else if ((BOOT_ACTIVE) == m_bootState)
        {
            console_task();

            if (0 != boot_task())
            {
                blinkTimeout = 300;
            }
        }
        else if ((BOOT_FINISH) == m_bootState)
        {
            inhex_finalize();
            m_bootState = BOOT_JUMP;
        }
        else if ((BOOT_JUMP) == m_bootState)
        {
            appTag = *((uint32_t *)((0xBD000000UL | (CFG_PROG_ADDR_START)) + 0x20UL));

            if (0x4E49414D != appTag)
            {
                /* no main; wait indefinitely for the programmer */
                m_bootState = BOOT_ACTIVE;
                blinkTimeout = 1800;
            }
            else
            {
                /* disable modules and chain to main */
                Console.Uninit();
                sys_uninit();
                chain();                    /* jump to the startup routine for the main app */
                __builtin_unreachable();    /* chain() cannot return since the stack is reset */
            }
        }

        if (0 == phase)
        {
            if (sys_timeout_ms(blinkTimer, 200))
            {
                blinkTimer = sys_millisec_time();
                phase = 1;
                PORTHCLR = (1 << 2);    /* green LED off */
            }
        }
        else if (sys_timeout_ms(blinkTimer, blinkTimeout))
        {
            blinkTimer = sys_millisec_time();
            phase = 0;
            PORTHSET = (1 << 2);    /* green LED on */
        }
    }

    sys_reset();
    return 0;
}


/**
 * @Function boot_task
 * @detail   Receives and processes data from the remote bootloader application.
 * @return   1 if <SOH> was detected, else 0.
 */
static int32_t  boot_task(void)
{
    static bool dle = false;
    static bool soh = false;
    int32_t result = 0;
    int32_t tok = Console.Getc();

    while (tok >= 0)
    {
        if ((true == soh) || (TOK_SOH == tok))
        {
            if (true == dle)
            {
                m_input[m_index++] = (uint8_t)tok;
                dle = false;
            }
            else if (TOK_SOH == tok)
            {
                m_index = 0;
                soh = true;
                result = 1;
                break;  /* break to notify caller that SOH was detected */
            }
            else if (TOK_DLE == tok)
            {
                dle = true;
            }
            else if (TOK_EOT == tok)
            {
                process_pkt();
                m_index = 0;
                soh = false;
                break;  /* break to allow caller to carry out any special processing */
            }
            else
            {
                m_input[m_index++] = (uint8_t)tok;
            }

            if (m_index >= 512)
            {
                /* overflow; attempt a resync */
                m_index = 0;
                soh = false;
            }
        }

        tok = Console.Getc();
    } /* process incoming packet data */

    return result;
} /* boot_task */


/**
 * @Function process_pkt
 * @detail   Processes a completed packet.
 */
static void  process_pkt(void)
{
    /* check the CRC */
    if (m_index > 2)
    {
        int32_t len = m_index - 2;
        uint16_t crc_calc = CalculateCrc(0, m_input, (uint32_t)len);
        uint16_t crc_pkt = (uint16_t)((m_input[len + 1] << 8) | (m_input[len]));

        if (crc_calc == crc_pkt)
        {
            if (0x01 == m_input[0])
            {
                process_version();
            }
            else if (0x02 == m_input[0])
            {
                process_erase();
            }
            else if (0x03 == m_input[0])
            {
                process_write();
            }
            else if (0x04 == m_input[0])
            {
                process_crc();
            }
            else if (0x05 == m_input[0])
            {
                /* special case: no packet response; finish programming and jump */
                m_bootState = BOOT_FINISH;
            }
        }
    }

    return;
} /* process_pkt */


/**
 * @Function process_version
 * @detail   Handles a Get Version command.
 */
static void  process_version(void)
{
    if (3 == m_index)
    {
        uint8_t msg[3];
        msg[0] = 1;
        msg[1] = BOOT_MAJOR;
        msg[2] = BOOT_MINOR;
        send_response(msg, 3);
    }

    return;
} /* process_version */


/**
 * @Function process_erase
 * @detail   Handles an Erase command.
 */
static void  process_erase(void)
{
    if (3 == m_index)
    {
        (void)inhex_erase();
        uint8_t msg[1] = { 2 };
        send_response(msg, 1);
    }

    return;
} /* process_erase */


/**
 * @Function process_crc
 * @detail   Handles a Calculate CRC command.
 */
static void  process_crc(void)
{
    if (11 <= m_index)
    {
        uint32_t addr = ((m_input[1]) | (((uint32_t)m_input[2]) << 8)
                        | (((uint32_t)m_input[3]) << 16) | (((uint32_t)m_input[4]) << 24));
        uint32_t len = ((m_input[5]) | (((uint32_t)m_input[6]) << 8)
                        | (((uint32_t)m_input[7]) << 16) | (((uint32_t)m_input[8]) << 24));
        uint16_t crc = (uint16_t)inhex_calc_crc(addr, len);
        uint8_t msg[3];
        msg[0] = 4;
        msg[1] = (uint8_t)(crc);
        msg[2] = (uint8_t)(crc >> 8);
        send_response(msg, 3);
    }

    return;
} /* process_crc */


/**
 * @Function process_write
 * @detail   Handles a Program command.
 */
static void  process_write(void)
{
    int32_t result = E_FAIL;

    if (8 <= m_index)
    {
        result = 0;
        int32_t payLen = m_index - 2;
        int32_t payIdx = 1;

        /* identify records and pass on for processing */
        while ((0 == result) && (payIdx < payLen))
        {
            int32_t recLen = m_input[payIdx] + 5;

            /* process the record if it fits within the actual payload */
            if ((payIdx + recLen) <= payLen)
            {
                result = inhex_process(&m_input[payIdx], recLen);
            }

            payIdx += recLen;
        }
    }

    uint8_t msg[1] = { 3 };
    send_response(msg, 1);

    return;
} /* process_write */


/**
 * @Function send_response
 * @detail   Performs DLE stuffing and transmits a response packet.
 * @param    msg [IN] is the response packet to send, excluding CRC.
 * @param    length [IN] is the number of bytes in msg.
 */
static void  send_response(uint8_t const *msg, uint32_t length)
{
    if ((0 != msg) && (0 != length))
    {
        uint8_t  response[128];
        uint16_t crc = CalculateCrc(0, msg, length);

        uint32_t idx = 1;
        response[0] = TOK_SOH;

        while ((0 < length) && (idx < 125))
        {
            --length;
            uint8_t tok = *msg++;

            if (((TOK_SOH) == tok) || ((TOK_EOT) == tok) || ((TOK_DLE) == tok))
            {
                response[idx++] = TOK_DLE;
            }

            response[idx++] = tok;
        }

        /* CRC is presented LSB first */
        response[idx++] = (uint8_t)(crc);
        response[idx++] = (uint8_t)(crc >> 8);
        response[idx++] = TOK_EOT;
        (void)UART2.Send(response, idx);
    }

    return;
} /* send_response */
