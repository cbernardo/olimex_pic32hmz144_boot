/*
 * File  : sys_util.c
 * Date  : 15 Aug 2020
 * Description: various system tools for the bootloader
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <xc.h>
#include <sys/attribs.h>
#include <stdint.h>

#include "sys_util.h"
#include "cfg_interrupts.h"

extern void _reset(void);


/******************************************************************************/
/*               PRIVATE VARIABLES                                            */
/******************************************************************************/
static uint32_t m_millisecTime;


/******************************************************************************/
/*               FUNCTION DECLARATIONS                                        */
/******************************************************************************/
void  Timer1_Handler(void);



/******************************************************************************/
/*               PUBLIC FUNCTIONS                                             */
/******************************************************************************/

/**
 * @Function sys_setup
 * @detail   Completes startup initialization and enables the system timer.
 */
void  sys_setup(void)
{
    static int32_t once = 0;

    if (0 == once)
    {
        once = 1;

        /* PRECON - Set up prefetch */
        PRECONbits.PFMSECEN = 0;    /* Flash SEC Interrupt Enable (0 = disabled) */
        PRECONbits.PREFEN = 0b11;   /* Predictive Prefetch Enable (b11 = enabled for any address) */
        PRECONbits.PFMWS = 0b011;   /* PFM Wait States (3 cycles, Erratum 38 DS80000663L, Silicon Rev B2) */

        /* Enable KSEG0 cache */
        uint32_t cp0 = _mfc0(16, 0);
        cp0 &= ~0x07UL;
        cp0 |= 0b011UL; /* K0 = Cacheable, non-coherent, write-back, write allocate */
        _mtc0(16, 0, cp0);

        /* set the shadow register sets for lower interrupt latency */
        PRISSbits.PRI7SS = 7;
        PRISSbits.PRI6SS = 6;
        PRISSbits.PRI5SS = 5;
        PRISSbits.PRI4SS = 4;
        PRISSbits.PRI3SS = 3;
        PRISSbits.PRI2SS = 2;
        PRISSbits.PRI1SS = 1;

        /* select multi-vectored interrupts and enable the global interrupt flag */

        /* Set CP0.CAUSE.IV = 1 */
        unsigned int cause = _CP0_GET_CAUSE();
        cause |= 0x00800000;
        _CP0_SET_CAUSE(cause);
        _ehb();

        /* Select multi-vectored interrupts */
        INTCONbits.MVEC = 1;

        sys_ie_enable(1);

        /*
         * Initialize Timer 1 for 1ms interrupts
         * Registers:
         *   T1CON = Control
         *   TMR1  = Value
         *   PR1   = MAX period
         *   IFS0<4> = Interrupt Flag Set
         *   IEC0<4> = Interrupt Enable Configuration
         *   IPC1<4:2> = IPL
         *   IPC1<1:0> = IPS
         *   Required prescaler: 0x01 = 1:8
         */
        T1CON = 0;
        T1CONbits.TCKPS = 1;
        PR1 = (uint32_t)((sys_get_pbclk() / (1000 * 8)) - 1);
        TMR1 = 0;
        IPC1bits.T1IP = IPL_TIMER1;
        IPC1bits.T1IS = IPS_TIMER1;
        IFS0bits.T1IF = 0;
        T1CONbits.ON = 1;
        IEC0bits.T1IE = 1;
    }

    return;    
} /* sys_setup */


/**
 * @Function sys_uninit
 * @detail   Shuts down modules in preparation for a chain to main.
 */
void  sys_uninit(void)
{
    T1CON = 0;
    IEC0bits.T1IE = 0;
    sys_ie_disable(0);
    return;
}

/**
 * @Function sys_reset
 * @detail   Performs a soft reset of the microcontroller.
 */
void __attribute__((__noreturn__)) sys_reset(void)
{
    _reset();
    __builtin_unreachable();    /* reset() does not return */
} /* sys_reset */


/**
 * @Function sys_ie_disable
 * @detail   Disables the Global Interrupt Enable flag.
 * @param    ieflag is a pointer to receive the current flag value; may be 0 if the current
 *           value is not required.
 */
void  sys_ie_disable(uint32_t *ieflag)
{
    if (0 != ieflag)
    {
        *ieflag = __builtin_disable_interrupts();
    }
    else
    {
        __builtin_disable_interrupts();
    }

    return;
} /* sys_ie_disable */


/**
 * @Function sys_ie_enable
 * @detail   Enables the Global Interrupt Enable flag.
 * @param    ieflag must be non-zero to enable interrupts.
 */
void  sys_ie_enable(uint32_t const ieflag)
{
    if (0 != ieflag)
    {
        __builtin_enable_interrupts();
    }

    return;
} /* sys_ie_enable */


/**
 * @Function sys_millisec_time
 * @detail   Returns the value of the system millisecond timer.
 */
uint32_t  sys_millisec_time(void)
{
    return m_millisecTime;
} /* sys_millisec_time */


/**
 * @Function sys_millisec_time
 * @detail   Returns the value of the system millisecond timer.
 */
bool  sys_timeout_ms(uint32_t const startTime, uint32_t const timeout)
{
    bool result = false;

    if ((m_millisecTime - startTime) >= timeout)
    {
        result = true;
    }

    return result;
} /* sys_timeout_ms */


/**
 * @Function sys_addr_to_phy
 * @detail   Returns a physical address given a virtual address in KSEG0 or KSEG1.
 */
void *sys_addr_to_phy(void const *addr)
{
    /*
     * Note: this conversion assumes that EBI and SQI mapping are not used
     */
    return (void *)(((uint32_t)addr) & 0x1FFFFFFF);
} /* sys_addr_to_phy */


/**
 * @Function Timer1_Handler
 * @detail   Interrupt Service Routine for the system millisecond timer.
 */
void  __ISR(_TIMER_1_VECTOR, SRS_TIMER1) Timer1_Handler(void)
{
    IFS0bits.T1IF = 0;
    ++m_millisecTime;
    return;
} /* Timer1_Handler */
