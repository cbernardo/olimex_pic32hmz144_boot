/*
 * File  : nvmem.c
 * Date  : 17 Aug 2020
 * Description: non-volatile memory controller for Program Flash.
 *
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdint.h>

#include "cfg_programmer.h"
#include "console.h"
#include "nvmem.h"
#include "sys_err.h"

#if 0
#define DBG_NVMEM
#endif

/*
 * Implementation notes:
 * + Program Word is not supported since it is assumed FLASH ECC will be used.
 * + Addresses outside the unprotected Program Flash range are ignored.
 */

/* Program Flash Operations */
#define NVOP_NOOP           (0x00)
#define NVOP_PROG_WORD      (0x01)  /* included for completeness; not used since ECC is assumed */
#define NVOP_PROG_QWORD     (0x02)
#define NVOP_PROG_ROW       (0x03)
#define NVOP_ERASE_PAGE     (0x04)
#define NVOP_ERASE_BANK0    (0x05)
#define NVOP_ERASE_BANK1    (0x06)
#define NVOP_ERASE_PROG     (0x07)

static int32_t  nvm_execute_op(void);
static int32_t  nvm_execute_op(void)
{
    NVMCONbits.WREN = 1;
    NVMKEY = 0;
    NVMKEY = 0xAA996655;
    NVMKEY = 0x556699AA;
    NVMCONSET = (1UL <<  15);
    _nop();
    _nop();
    _nop();
    _nop();

    while (NVMCONbits.WR)
    {
        ; /* wait for operation to finish */
    }

    NVMCONbits.WREN = 0;
    int32_t result = 0;

    if (0 != (NVMCON & (1UL << 13)))
    {
        #ifdef DBG_NVMEM
        Console.Puts("! WRERR set\n");
        Console.FlushTx();
        #endif
        result = E_FAIL;
    }

    if (0 != (NVMCON & (1UL << 12)))
    {
        #ifdef DBG_NVMEM
        Console.Puts("! LVDERR set\n");
        Console.FlushTx();
        #endif
        result = E_FAIL;
    }

    return result;
}

/**
 * @Function nvm_erase_page
 * @detail   erases a single page
 * @param    addr is an address within the page to be erased.
 * @return   0 on success, otherwise E_FAIL
 */
int32_t  nvm_erase_page(uint32_t addr)
{
    /* note: PIC32MZ does not have a user controllable option to
     * verify and retry a page erase operation
     */
    while (NVMCONbits.WR)
    {
        ; /* wait for previous operation to finish */
    }

    int32_t result = 0;

    if (NVMCONbits.LVDERR)
    {
        /* Low voltage detect was triggered */
        #ifdef DBG_NVMEM
        Console.Puts("! LVDERR set\n");
        Console.FlushTx();
        #endif
        result = E_FAIL;
    }

    if (0 == result)
    {
        if (NVMCONbits.WRERR)
        {
            nvm_nop();
        }

        addr = (0x1DFFFFFFUL & addr);
        NVMADDR = addr;
        NVMCONbits.NVMOP = NVOP_ERASE_PAGE;
        result = nvm_execute_op();
    }

    return result;
} /* nvm_erase_page */


/**
 * @Function nvm_erase_bank
 * @detail   erases the specified memory bank, verifies erasure and retries if necessary.
 * @param    bank is the memory bank to erase (0, 1).
 * @return   0 on success, otherwise E_FAIL
 */
int32_t  nvm_erase_bank(uint32_t bank)
{
    while (NVMCONbits.WR)
    {
        ; /* wait for previous operation to finish */
    }

    int32_t  result = 0;
    uint32_t bankStartAddr = (CFG_PROG_BANK0_ADDR);
    uint32_t bankEndAddr = (CFG_PROG_BANK0_ADDR);

    if (NVMCONbits.LVDERR)
    {
        /* Low voltage detect was triggered */
        #ifdef DBG_NVMEM
        Console.Puts("! LVDERR set\n");
        Console.FlushTx();
        #endif
        result = E_FAIL;
    }

    if ((0 != bank) && (1 != bank))
    {
        /* invalid bank */
        #ifdef DBG_NVMEM
        Console.Puts("! Invalid bank number\n");
        Console.FlushTx();
        #endif
        result = E_FAIL;
    }

    if (0 != (CFG_PROG_PAGE_WP))
    {
        if ((0 == bank) || ((CFG_PROG_ADDR_START) >= (CFG_PROG_BANK1_ADDR)))
        {
            /* Command cannot be used since it contains write-protected regions */
            #ifdef DBG_NVMEM
            Console.Puts("! Overlap with write protected region\n");
            Console.FlushTx();
            #endif
            result = E_FAIL;
        }
        else if ((1 == bank) && ((CFG_PROG_ADDR_END) <= (CFG_PROG_BANK1_ADDR)))
        {
            /* Bank 1 is outside the specified program end address */
            #ifdef DBG_NVMEM
            Console.Puts("! Address is greater than Program End Address\n");
            Console.FlushTx();
            #endif
            result = E_FAIL;
        }
    }

    if (0 == result)
    {
        if (NVMCONbits.WRERR)
        {
            nvm_nop();
        }

        if (0 == bank)
        {
            NVMCONbits.NVMOP = NVOP_ERASE_BANK0;
        }
        else
        {
            NVMCONbits.NVMOP = NVOP_ERASE_BANK1;
            bankStartAddr = (CFG_PROG_BANK1_ADDR);
            bankEndAddr = (CFG_PROG_ADDR_END) + 1UL;
        }

        result = nvm_execute_op();
    }

    if (0 == result)
    {
        /* verify every page and reprogram defective pages */
        uint32_t startPAddr = bankStartAddr;
        uint32_t startVAddr = 0xA0000000UL | bankStartAddr;         /* non-cached address */

        while (startPAddr < bankEndAddr)
        {
            uint32_t startEvict = (0x9FFFFFFFUL & startVAddr);      /* cacheable address */
            uint32_t endEvict = startEvict + (CFG_NVMEM_SIZE_PAGE);

            /* evict cache lines in the current page */
            while (startEvict < endEvict)
            {
                _cache(0x11, (void *)startEvict);
                _sync();
                startEvict += 16UL;
            }

            uint32_t *pData = (uint32_t *)startVAddr;       /* non-cached address */
            uint32_t nData = ((CFG_NVMEM_SIZE_PAGE) >> 2);  /* number of uint32_t in a page */
            uint32_t check = 0xFFFFFFFFUL;

            while (0 != nData)
            {
                check &= *pData++;
                --nData;
            }

            if (0xFFFFFFFFUL != check)
            {
                /* not correctly erased; attempt a page erase */
                result = nvm_erase_page(startPAddr);
            }

            /* set addresses to the next page */
            startVAddr += (CFG_NVMEM_SIZE_PAGE);
            startPAddr += (CFG_NVMEM_SIZE_PAGE);
        }
    }

    return result;
} /* nvm_erase_bank */


/**
 * @Function nvm_erase_prog
 * @detail   erases all program memory, verifies erasure and retries if necessary.
 * @return   0 on success, otherwise E_FAIL
 */
int32_t  nvm_erase_prog(void)
{
    while (NVMCONbits.WR)
    {
        ; /* wait for previous operation to finish */
    }

    int32_t result = 0;

    if (NVMCONbits.LVDERR)
    {
        /* Low voltage detect was triggered */
        #ifdef DBG_NVMEM
        Console.Puts("! LVDERR set\n");
        Console.FlushTx();
        #endif
        result = E_FAIL;
    }

    if (0 != (CFG_PROG_PAGE_WP))
    {
        /* Command cannot be used since it contains write-protected regions */
        #ifdef DBG_NVMEM
        Console.Puts("! Overlaps write protected region\n");
        Console.FlushTx();
        #endif
        result = E_FAIL;
    }

    if (0 == result)
    {
        if (NVMCONbits.WRERR)
        {
            nvm_nop();
        }

        NVMCONbits.NVMOP = NVOP_ERASE_PROG;
        result = nvm_execute_op();
    }

    if (0 == result)
    {
        /* verify every page and reprogram defective pages */
        uint32_t startPAddr = (CFG_PROG_ADDR_START);
        uint32_t startVAddr = 0xA0000000UL | (CFG_PROG_ADDR_START); /* non-cached address */

        while (startPAddr < (CFG_PROG_ADDR_END))
        {
            uint32_t startEvict = (0x9FFFFFFFUL & startVAddr);      /* cacheable address */
            uint32_t endEvict = startEvict + (CFG_NVMEM_SIZE_PAGE);

            /* evict cache lines in the current page */
            while (startEvict < endEvict)
            {
                _cache(0x11, (void *)startEvict);
                _sync();
                startEvict += 16UL;
            }

            uint32_t *pData = (uint32_t *)startVAddr;       /* non-cached address */
            uint32_t nData = ((CFG_NVMEM_SIZE_PAGE) >> 2);  /* number of uint32_t in a page */
            uint32_t check = 0xFFFFFFFFUL;

            while (0 != nData)
            {
                check &= *pData++;
                --nData;
            }

            if (0xFFFFFFFFUL != check)
            {
                /* not correctly erased; attempt a page erase */
                result = nvm_erase_page(startPAddr);
            }

            /* set addresses to the next page */
            startVAddr += (CFG_NVMEM_SIZE_PAGE);
            startPAddr += (CFG_NVMEM_SIZE_PAGE);
        }
    }

    return result;
} /* nvm_erase_prog */


/**
 * @Function nvm_prog_row
 * @detail   programs the specified row of data.
 * @param    addr is a Physical Address to the start of the row.
 * @param    data is a pointer to the program data; must be a coherent KSEG1 address and aligned to
 *           a cache line (16B).
 * @return   0 on success, otherwise E_FAIL
 */
int32_t  nvm_prog_row(uint32_t addr, void const *data)
{
    int32_t result = 0;
    // program a row of data
    // + check that addr is 0x1Dxxxxxx
    // + check that data is 0xA0xxxxxx then cast to PHY (0x00xxxxxx)

    if (0 != (addr & ((CFG_NVMEM_SIZE_ROW) - 1)))
    {
        #ifdef DBG_NVMEM
        Console.Puts("! destination not aligned to a row\n");
        Console.FlushTx();
        #endif
        result = E_FAIL;    /* destination not aligned to a row */
    }

    uint32_t srcAddr = (uint32_t)data;

    if ((0xA0000000UL != (srcAddr & 0xFF000000UL)) || (0 != (srcAddr & 0x0FUL)))
    {
        #ifdef DBG_NVMEM
        Console.Puts("! source not in KSEG1 or not aligned to cache line\n");
        Console.FlushTx();
        #endif
        result = E_FAIL;    /* source not in KSEG1 or not aligned to a cache line */
    }

    if (0x1D000000 != (addr & 0xFF000000UL))
    {
        #ifdef DBG_NVMEM
        Console.Puts("! Destination not in physical Program Memory\n");
        Console.FlushTx();
        #endif
        result = E_FAIL;    /* destination not in physical program memory */
    }

    if (NVMCONbits.LVDERR)
    {
        /* Low voltage detect was triggered */
        #ifdef DBG_NVMEM
        Console.Puts("! LVDERR set\n");
        Console.FlushTx();
        #endif
        result = E_FAIL;
    }

    if (0 == result)
    {
        srcAddr &= 0x1FFFFFFFUL;
        #ifdef DBG_NVMEM
        Console.Puts("* Copying data from ");
        Console.Print32H(srcAddr);
        Console.Puts(" to ");
        Console.Print32H(addr);
        Console.Puts("\n");
        Console.FlushTx();
        #endif

        while (NVMCONbits.WR)
        {
            ; /* wait for previous operation to finish */
        }

        if (NVMCONbits.WRERR)
        {
            nvm_nop();
        }

        NVMSRCADDR = srcAddr;                   /* PHY address for non-cached source */
        NVMADDR = addr;                         /* PHY address for Program Memory destination */
        NVMCONbits.NVMOP = NVOP_PROG_ROW;
        result = nvm_execute_op();
    }

    return result;
} /* nvm_prog_row */


/**
 * @Function nvm_nop
 * @detail   Executes a memory controller NOP to clear the WRERR flag
 */
void  nvm_nop(void)
{
    while (NVMCONbits.WR)
    {
        ; /* wait for previous operation to finish */
    }

    NVMCONbits.NVMOP = NVOP_NOOP;
    (void) nvm_execute_op();

    return;
} /* nvm_nop */
